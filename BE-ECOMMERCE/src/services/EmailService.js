const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
const inlineBase64 = require('nodemailer-plugin-inline-base64');
dotenv.config();

const sendEmailCreateOrder = async (email, orderItems) => {
    try {
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true,
            auth: {
                user: process.env.MAIL_ACCOUNT,
                pass: process.env.MAIL_PASSWORD,
            },
        });

        transporter.use('compile', inlineBase64({ cidPrefix: 'somePrefix_' }));

        let listItem = '';
        const attachImage = [];
        
        orderItems.forEach((order) => {
            listItem += `<div>
                <div>Bạn đã đặt sản phẩm <b>${order.name}</b> với số lượng: <b>${order.amount}</b> và giá là: <b>${order.price} VND</b></div>
                <div>Bên dưới là hình ảnh của sản phẩm</div>
            </div>`;
            attachImage.push({ path: order.image }); // Assuming order.image is the correct path to the image
        });

        const mailOptions = {
            from: process.env.MAIL_ACCOUNT,
            to: email,
            subject: "Bạn của bạn tại shop DEVCAMPSHOP.24H",
            text: "Hello world?",
            html: `<div><b>Bạn đã đặt hàng thành công tại shop DEVCAMPSHOP.24H</b></div> ${listItem}`,
            attachments: attachImage,
        };

        let info = await transporter.sendMail(mailOptions);
        console.log("Email sent:", info.response);
    } catch (error) {
        console.error("Error sending email:", error);
        throw new Error("Failed to send email");
    }
};

module.exports = {
    sendEmailCreateOrder
};
