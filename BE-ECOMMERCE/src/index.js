const express = require('express');
const dotenv = require('dotenv');
const mongoose = require("mongoose");
const routes = require('./routes')
const cors = require('cors');
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

dotenv.config();

const app = express();
const port = process.env.ENV_PORT || 3001;

app.use(cors())
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));
app.use(bodyParser.json())
app.use(cookieParser())

routes(app);

// Kết nối tới cơ sở dữ liệu MongoDB
mongoose.connect(process.env.MONGO_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => {
    console.log('Connected to MongoDB Atlas!');
})
.catch((error) => {
    console.error('Error connecting to MongoDB:', error);
});

// Route chính
app.get('/', (request, response) => {
    response.json(
        {
            message: `Welcome to JWT ${new Date()}`
        });
});

// Lắng nghe các kết nối đến cổng đã chọn
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
