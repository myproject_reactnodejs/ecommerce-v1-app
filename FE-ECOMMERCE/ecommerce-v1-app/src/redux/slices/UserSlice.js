import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    name: '',
    email: '',
    phone: '',
    address: '',
    avatar: '',
    access_token: '',
    id: '',
    isAdmin: false,
    city: '',
    refreshToken: ''
};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: { 
        updateUser: (state, action) => {
            const { userData, access_token, refreshToken  } = action.payload || {};
            if (userData) {
                const { name = '', email = '', address = '', phone = '', avatar = '', _id = '', isAdmin, city= '' } = userData;
                state.name = name ? name : state.name;
                state.email = email ? email : state.email;
                state.address = address ? address : state.address;
                state.phone = phone ? phone : state.phone;
                state.avatar = avatar ? avatar : state.avatar;
                state.id = _id ? _id : state.id
                state.access_token = access_token ? access_token : state.access_token;
                state.isAdmin = isAdmin !== undefined ? isAdmin : state.isAdmin;
                state.city = city ? city : state.city;
            }
            state.refreshToken = refreshToken ? refreshToken : state.refreshToken;
        },
        
        resetUser: (state) => {
            state.name = '';
            state.email = '';
            state.address = '';
            state.phone = '';
            state.avatar = '';
            state.id = '';
            state.access_token = '';
            state.isAdmin = false;
            state.city = '';
            state.refreshToken = ''
            
        },
    }  
});

// Export action creators
export const { updateUser, resetUser  } = userSlice.actions;

// Export reducer
export default userSlice.reducer;
