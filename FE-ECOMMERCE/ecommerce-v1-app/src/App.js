import React, { Fragment, useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import { routes } from './routes';
import DefaultComponent from './components/DefaultComponent/DefaultComponent';
import * as UserService from './services/UserService'
import { jwtDecode } from 'jwt-decode';
import { isJsonString } from './utils';
import { useDispatch, useSelector } from 'react-redux';
import { updateUser } from './redux/slices/UserSlice'
import Loading from './components/LoadingComponent/Loading';

function App() {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false)
  const user = useSelector((state) => state.user)

  useEffect(() => {
    setIsLoading(true)
    const { storageData, decoded } = handleDecoded()
    if (decoded?.id) {
      handleGetDetailsUser(decoded?.id, storageData, dispatch)
    }
    setIsLoading(false)
  }, [])

  const handleDecoded = () => {
    let storageData = sessionStorage.getItem('access_token')
    let decoded = {}
    if (storageData && isJsonString(storageData)) {
      storageData = JSON.parse(storageData)
      decoded = jwtDecode(storageData)
    }
    return { decoded, storageData }
  }

  UserService.axiosJWT.interceptors.request.use(async (config) => {
    const currentTime = new Date()
    const { decoded } = handleDecoded()
    let storageRefreshToken = sessionStorage.getItem('refresh_token')
    const refreshToken = JSON.parse(storageRefreshToken)
    const decodedRefreshToken =  jwtDecode(refreshToken)
    if (decoded?.exp < currentTime.getTime() / 1000) {
      if(decodedRefreshToken?.exp > currentTime.getTime() / 1000) {
        const data = await UserService.refreshToken(refreshToken)
        config.headers['token'] = `Bearer ${data?.access_token}`
      }

    }
    return config;
  }, (err) => {
    return Promise.reject(err)
  })

  const handleGetDetailsUser = async (id, token, dispatch) => {
    try {
        const storage = sessionStorage.getItem('refresh_token')
        const refreshToken = JSON.parse(storage)
        const res = await UserService.getDetailsUser(id, token);// Gọi UserService để lấy chi tiết người dùng
        // Kiểm tra và sử dụng dữ liệu trả về từ UserService nếu cần thiết
        // console.log("res", res);
        dispatch(updateUser({userData: res.data, access_token: token, refreshToken: refreshToken}));// Dispatch action updateUser với dữ liệu từ res
    } catch (error) {
        console.error("Error while getting user details:", error);
    }
  };

  return (
    <div style={{height: '100vh', width: '100%'}}>
      <Loading isLoading={isLoading}>
        <Router>
          <Routes>
            {routes.map((route) => {
              const Page = route.page;
              const isCheckAuth = !route.isPrivated || user.isAdmin;
              const Layout = route.isShowHeader ? DefaultComponent : Fragment;
              return (
                <Route 
                  key={route.path} 
                  path={route.path} 
                  element={
                    <Layout>
                      {isCheckAuth ? <Page /> : <Navigate to="/sign-in" />} {/*Thêm điều kiện isCheckAuth*/}
                    </Layout>
                  }
                />
              );
            })}
          </Routes>
        </Router>
      </Loading>
    </div>
  )
}

export default App;