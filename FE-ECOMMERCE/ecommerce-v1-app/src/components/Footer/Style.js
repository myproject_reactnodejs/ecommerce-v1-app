import React from 'react';
import styled from 'styled-components';

export const FooterContainer = styled.footer`
  width: 100%;
  padding: 20px;
  background-color: #f8f9fa;
  text-align: center;
  position: fixed;
  bottom: 0;
`