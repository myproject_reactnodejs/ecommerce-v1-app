import React from 'react'
import { FooterContainer } from './Style';

const Footer = () => {
  return (
    <FooterContainer>
    <p>© 2024 Welcome to DevcampShop.24h</p>
    </FooterContainer>
  )
}

export default Footer