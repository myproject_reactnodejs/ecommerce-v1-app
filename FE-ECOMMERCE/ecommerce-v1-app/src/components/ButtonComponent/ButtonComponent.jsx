import React from 'react';
import { Button } from 'antd';
import './ButtonComponent.css';

const ButtonComponent = ({ size, styleButton, styleTextButton, textbutton, disabled, onClick, ...rests }) => {
  
  return (
    <Button
      className="custom-button"
      style={{
        ...styleButton,
        background: disabled ? '#ccc' : styleButton?.background 
      }}
      size={size}
      {...rests}
      onClick={onClick}
      disabled={disabled}
    >
      <span style={styleTextButton}>{textbutton}</span>
    </Button>
  );
};

export default ButtonComponent;
