import React from 'react';
import { WrapperContent, 
    WrapperLableText, 
    WrapperTextPrice, 
    WrapperTextValue } from './Style';
import { Checkbox, Divider, Rate } from 'antd';

const NavbarComponent = () => {
    const onChange = () => { }
    const renderContent = (type, options) => {
            switch (type) {
                case 'text':
                    return options.map((option) => {
                        return (
                            <WrapperTextValue>{option}</WrapperTextValue>
                        )
                    })
                case 'checkbox':
                    return (
                        <Checkbox.Group style={{ width: '100%', display: 'flex', flexDirection: 'column', gap: '12px' }} onChange={onChange}>
                            {options.map((option) => {
                                return (
                                    <Checkbox style={{ marginLeft: 0 }} value={option.value}>{option.label}</Checkbox>
                            )
                            })}
                        </Checkbox.Group>
                    )
                case 'star':
                    return options.map((option) => {
                        return (
                            <div style={{ dispaly: 'flex' }}>
                                <Rate style={{ fontSize: '12px' }} disabled defaultValue={option} />
                                <span> {`from ${option}  star`}</span>
                            </div>
                        )
                    })
                case 'price':
                    return options.map((option) => {
                        return (
                            <WrapperTextPrice>{option}</WrapperTextPrice>
                        )
                    })
                    default:
                        return {}
        }
    }
  return (
    <div style={{ backgroundColor:'#fff', padding: '10px'}}>
        <WrapperLableText>Filter Options</WrapperLableText>
        <Divider />
        <WrapperContent>
            {renderContent('text', ['Tu Lanh', 'TV', 'May Giat'])}
        </WrapperContent>
        <Divider />
        <WrapperContent>
            {renderContent('checkbox', [
                { value:'a', label: 'A'},
                { value:'b', label: 'B'}, 
                { value:'c', label: 'C'}
            ])}
        </WrapperContent>
        <Divider />
        <WrapperContent>
            {renderContent('star', [3, 4, 5])}
        </WrapperContent>
        <Divider />
        <WrapperContent>
            {renderContent('price', ['< 100.000VND', '< 300.000VND', '< 500.000VND'])}
        </WrapperContent>
    </div>
  )
}

export default NavbarComponent