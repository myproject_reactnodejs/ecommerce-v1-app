import { Button, Input, Modal, Table } from 'antd';
import {  DownloadOutlined  } from '@ant-design/icons'
import React, { useState } from 'react'
import Loading from '../../components/LoadingComponent/Loading'
import { Excel } from "antd-table-saveas-excel";
import { useMemo } from 'react';

const TableComponent = (props) => {
    const { selectionType = 'checkbox', data:dataSource =[], isLoading = false, columns=[], handleDelteMany } = props;

      // rowSelection object indicates the need for row selection
      const [rowSelectedKeys, setRowSelectedKeys] = useState([])
      const [isExportModalVisible, setIsExportModalVisible] = useState(false);
      const [exportFileName, setExportFileName] = useState('');
      const [size, setSize] = useState('large')

      const newColumnExport = useMemo(() => {
        const arr = columns?.filter((col) => col.dataIndex !== 'action')
        return arr
      }, [columns])

      const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
          setRowSelectedKeys(selectedRowKeys)
        },
      
      };
      const handleDeleteAll = () => {
        handleDelteMany(rowSelectedKeys)
      }

      const exportExcel = () => {
            setIsExportModalVisible(true);
        };

        const handleExportConfirm = () => {
            const excel = new Excel();
            excel
                .addSheet("test")
                .addColumns(newColumnExport)
                .addDataSource(dataSource, {
                    str2Percent: true
                })
                .saveAs(`${exportFileName}.xlsx`);
            setIsExportModalVisible(false);
        };

  return (
    <div>
      <Loading isLoading={isLoading}>
        {!!rowSelectedKeys.length && (
          <div style={{
            background: 'rgb(26, 148, 255)',
            color: '#fff',
            fontWeight: 'bold',
            padding: '10px',
            cursor: 'pointer'
          }}
            onClick={handleDeleteAll}
          >
            Xóa tất cả
          </div>
        )}
        {/* <button onClick={exportExcel}>Export Excel</button> */}
        <Button 
          type="primary" 
          icon={<DownloadOutlined />} 
          onClick={exportExcel} 
          size={size}
          style={{ marginBottom:'20px'}}>
            Export Excel
          </Button>
        <Table
            rowSelection={{
                type: selectionType,
                ...rowSelection,
            }}
            columns={columns}
            dataSource={dataSource}
            {...props}
        />
      </Loading>
      <Modal
          title="Export Excel"
          open={isExportModalVisible}
          onOk={handleExportConfirm}
          onCancel={() => setIsExportModalVisible(false)}
      >
          <Input
              placeholder="Enter file name"
              value={exportFileName}
              onChange={(e) => setExportFileName(e.target.value)}
          />
          {/* Add more components for file path selection if needed */}
      </Modal>

    </div>
  )
}

export default TableComponent