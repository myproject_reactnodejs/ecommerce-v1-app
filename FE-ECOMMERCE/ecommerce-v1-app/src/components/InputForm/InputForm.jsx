import React from 'react'
import { WrapperInputStyle } from './Style'

const InputForm = (props) => {
    const { placeholder = 'Nhập text', onChange, ...rests } = props; // Destructure onChange from props
    const handleOnchangeInput = (e) => {
        if (typeof onChange === 'function') { // Check if onChange is a function
            onChange(e.target.value); // Call onChange if it's a function
        }
    };
    return (
        <WrapperInputStyle placeholder={placeholder} value={props.value} {...rests} onChange={handleOnchangeInput} />
    );
};

export default InputForm;

