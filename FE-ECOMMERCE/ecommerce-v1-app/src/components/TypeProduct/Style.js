import styled from "styled-components";

export const WrapperType = styled.div`
  padding: 0px 0px;
  cursor: pointer;
  &:hover {
    background-color: var(--primary-color);
    color: #fff;
    border-radius: 4px;
  }
`