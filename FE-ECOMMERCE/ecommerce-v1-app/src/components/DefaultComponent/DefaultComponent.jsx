import React from 'react'
import HeaderComponent from '../HeaderComponent/HeaderComponent'
import Footer from '../Footer/Footer'

const DefaultComponent = ({ children }) => {
  return (
    <div style={{ width:'100%'}}>
        <HeaderComponent />
        {children}
        <Footer />
    </div>
  )
}

export default DefaultComponent