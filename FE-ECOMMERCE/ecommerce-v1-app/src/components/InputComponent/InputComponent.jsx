import { Input } from 'antd'
import React from 'react'
import './InputComponent.css'

const InputComponent = ({size, placeholder, style, ...rests }) => {
  return (
    <Input
        className='custom-input' 
        size={size} 
        placeholder={placeholder} 
        variant="outlined"
        style={style}
        {...rests} 
    />
  )
}

export default InputComponent