import { Spin } from 'antd'
import React from 'react'

const Loading = ({ children, isLoading, deday = 200 }) => {
    return (
        <Spin tip="Loading" spinning={isLoading} delay={deday} size="large">
            {children}
        </Spin>
    )
}

export default Loading