import React, { useEffect, useState } from 'react';
import { Badge, Col, Popover } from 'antd';
import { WrapperContentPopup, WrapperHeader, 
  WrapperHeaderAccount, 
  WrapperTextHeader, 
  WrapperTextHeaderSmall } from './style';
import { UserOutlined, 
    CaretDownOutlined, 
    ShoppingCartOutlined 
  } from '@ant-design/icons';
import ButtonInputSearch from '../ButtonInputSearch/ButtonInputSearch';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { resetUser } from '../../redux/slices/UserSlice';
import * as UserService from '../../services/UserService'
import Loading from '../LoadingComponent/Loading';
import { searchProduct } from '../../redux/slices/productSlice';

const HeaderComponent = ({ isHiddenSearch = false, isHiddenCart = false}) => {

  const navigate = useNavigate()
  const dispatch = useDispatch();
  const [userName, setUserName] = useState('')
  const [userAvatar, setUserAvatar] = useState('')
  const user  = useSelector((state) => state.user) // state redux
  const order = useSelector((state) => state.order)
  const [loading, setLoading] = useState(false)
  const [search, setSearch] = useState('')
  const [isOpenPopup, setIsOpenPopup] = useState(false)

  const handleNavigateLogin = () => {
    navigate('/sign-in')
  }

  const handleLogout = async() => {
    setLoading(true)
    await UserService.loginUser()
    dispatch(resetUser());
    sessionStorage.removeItem('refresh_token')
    sessionStorage.removeItem('access_token')
    setLoading(false)
  }

  useEffect(() => {
    setLoading(true)
    setUserName(user?.name)
    setUserAvatar(user?.avatar)
    setLoading(false)
  }, [user?.name, user?.avatar])

  const content = (
    <div>
      <WrapperContentPopup onClick={() => navigate('/profile-user')}>User Information</WrapperContentPopup>
      {user?.isAdmin && (
        <WrapperContentPopup onClick={() => navigate('/system/admin')}>System Management</WrapperContentPopup>
      )}
      <WrapperContentPopup onClick={() => handleClickNavigate(`my-order`)}>My Order List</WrapperContentPopup>
      
      <WrapperContentPopup onClick={handleLogout}>Log out</WrapperContentPopup>
    </div>
  );

  const handleClickNavigate = (type) => {
    if(type === 'profile') {
      navigate('/profile-user')
    }else if(type === 'admin') {
      navigate('/system/admin')
    }else if(type === 'my-order') {
      navigate('/my-order',{ state : {
          id: user?.id,
          token : user?.access_token
        }
      })
    }else {
      handleLogout()
    }
    setIsOpenPopup(false)
  }

  const onSearch = (e) => {
    setSearch(e.target.value)
    // console.log("e", e.target.value)
    dispatch(searchProduct(e.target.value))
  }

  return (
    <div style={{  height: '100%', width: '100%', display: 'flex',background: 'rgb(26, 148, 255)', justifyContent: 'center' }}>
      <WrapperHeader style={{ justifyContent: isHiddenSearch && isHiddenSearch ? 'space-between' : 'unset' }}>
        <Col span={5}>
          <WrapperTextHeader to='/'>DEVCAMPSHOP.24H</WrapperTextHeader>
        </Col>
        {!isHiddenSearch && (
          <Col span={13}>
            <ButtonInputSearch 
              size="large"
              bordered={false}
              textbutton="Tìm kiếm"
              placeholder="input search text"
              backgroundColorButton="#5a20c1"
              onChange={onSearch}
            />
          </Col>
        )}
        <Col span={6} style={{ display: 'flex', gap: '54px', alignItems: 'center' }}>
          <Loading isLoading={loading}>
            <WrapperHeaderAccount>
              {userAvatar ? (
                  <img src={userAvatar} alt="avatar" style={{
                    height: '40px',
                    width: '40px',
                    borderRadius: '50%',
                    objectFit: 'cover'
                  }} />
                ) : (
                  <UserOutlined style={{ fontSize: '30px' }} />
                )}
              {user?.access_token ? (
                  <>
                    <Popover content={content} trigger="click" open={isOpenPopup}>
                      <div style={{ cursor: 'pointer',maxWidth: 100, overflow: 'hidden', textOverflow: 'ellipsis' }} 
                      onClick={() => setIsOpenPopup((prev) => !prev)}>{userName?.length ? userName : user?.email}</div>
                    </Popover>
                  </>
              ) : (
                <div onClick={handleNavigateLogin} style={{ cursor: 'pointer' }}>
                    <WrapperTextHeaderSmall>Sign in/Sign up</WrapperTextHeaderSmall> 
                    <div>
                      <WrapperTextHeaderSmall>Account</WrapperTextHeaderSmall>
                      <CaretDownOutlined />
                    </div>
                </div>
              )}
            </WrapperHeaderAccount>
          </Loading>
          {!isHiddenCart && (
            <div onClick={() => navigate('/order')} style={{cursor: 'pointer'}}>
              <Badge count={order?.orderItems?.length} size='small'>
                <ShoppingCartOutlined style={{ fontSize:'30px', color:'#fff'}}/>
              </Badge>
              <WrapperTextHeaderSmall>Cart</WrapperTextHeaderSmall>
            </div>
          )}
        </Col>
      </WrapperHeader>
    </div>
  )
}

export default HeaderComponent