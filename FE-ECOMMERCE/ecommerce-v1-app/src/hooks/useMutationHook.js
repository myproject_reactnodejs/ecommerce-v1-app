import { useEffect } from 'react';
import { useMutation } from "@tanstack/react-query";

export const useMutationHooks = (fnCallback) => {
    const mutation = useMutation({
        mutationFn: fnCallback
    });

    // useEffect(() => {
    //     console.log('isIdle:', mutation.isIdle);
    //     console.log('isLoading:', mutation.isLoading);
    //     console.log('isSuccess:', mutation.isSuccess);
    //     console.log('isError:', mutation.isError);
    // }, [mutation]);

    return {
        ...mutation
    };
};


