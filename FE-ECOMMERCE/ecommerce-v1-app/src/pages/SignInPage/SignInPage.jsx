import React, { useEffect, useState } from 'react'
import { WrapperContainerLeft, WrapperContainerRight, WrapperTextLight } from './Style'
import InputForm from '../../components/InputForm/InputForm'
import Loading from '../../components/LoadingComponent/Loading'
import ButtonComponent from '../../components/ButtonComponent/ButtonComponent'
import { Image } from 'antd'
import { EyeFilled, EyeInvisibleFilled } from '@ant-design/icons'
import imageLogo from '../../assets/images/logo-login.png'
import { useLocation, useNavigate } from 'react-router-dom'
import * as UserService from '../../services/UserService'
import { useMutationHooks } from '../../hooks/useMutationHook'
import { jwtDecode } from 'jwt-decode';
import { useDispatch } from 'react-redux';
import { updateUser } from '../../redux/slices/UserSlice'
import * as message from '../../components/Message/Message'
import validator from 'validator';

const SignInPage = () => {

  const navigate = useNavigate()

  const [isLoading, setIsLoading] = useState(false); //set state cho isLoading spin 
  
  const mutation = useMutationHooks(
    data => UserService.loginUser(data)
  )
  const { data, isSuccess } = mutation
 
  const [isShowPassword, setIsShowPassword] = useState(false);
  const location = useLocation()
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
 
  const handleNavigateSignUp = () => {
      navigate('/sign-up')
  }

  const handleOnchangeEmail = (value) => {
    setEmail(value)
  }

  const handleOnchangePassword = (value) => {
    setPassword(value)
  }

  const handleSignIn = async () => {
    setIsLoading(true); // Set isLoading thành true khi người dùng click vào nút đăng nhập
    if (!validator.isEmail(email)) {
      // Hiển thị thông báo lỗi cho người dùng
      message.error('Vui lòng nhập đúng định dạng email!');
      setIsLoading(false); // Set isLoading thành false để dừng loading
      return;
    }
    try {
      const response = await mutation.mutateAsync({
        email,
        password
      });
      if (data?.status === 'ERR') {
        setIsLoading(false); // Set isLoading thành false để dừng loading
        return;
      }
    } catch (error) {
      console.error('Error during login:', error);
      setIsLoading(false); // Set isLoading thành false để dừng loading
      return;
    }
    setTimeout(() => {
      setIsLoading(false); // Sau 1 giây, set isLoading thành false
    }, 1000);
};

  useEffect(() => {
    if (isSuccess && data?.status !== 'ERR') {
      if(location?.state) {
        navigate(location?.state)
      } else {
        navigate('/')
        message.success(data?.message);
      }
      sessionStorage.setItem('access_token', JSON.stringify(data?.access_token))
      sessionStorage.setItem('refresh_token', JSON.stringify(data?.refresh_token))
        if (data?.access_token) {
          const decoded = jwtDecode(data?.access_token)
          // console.log("decode",decoded)
          if (decoded?.id) {
            handleGetDetailsUser(decoded?.id, data?.access_token, dispatch)
          }
      }
    }
  }, [isSuccess, data?.status])

  const handleGetDetailsUser = async (id, token, dispatch) => {
    try {
        const storage = sessionStorage.getItem('refresh_token')
        const refreshToken = JSON.parse(storage)
        const res = await UserService.getDetailsUser(id, token);// Gọi UserService để lấy chi tiết người dùng
        // Kiểm tra và sử dụng dữ liệu trả về từ UserService nếu cần thiết
        // console.log("res", res);
        dispatch(updateUser({userData: res.data, access_token: token, refreshToken: refreshToken}));// Dispatch action updateUser với dữ liệu từ res
    } catch (error) {
        console.error("Error while getting user details:", error);
    }
};

  return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', background: 'rgba(0, 0, 0, 0.53)', height: '100vh' }}>
        <div style={{ width: '800px', height: '445px', borderRadius: '6px', background: '#fff', display: 'flex' }}>
            <WrapperContainerLeft>
              <h1>Xin chào</h1>
              <p>Đăng nhập Email vào tạo tài khoản</p>
              <form>
                <InputForm 
                  style={{ marginBottom: '10px' }} 
                  placeholder="abc@gmail.com" 
                  value={email} onChange={handleOnchangeEmail}
                  />
              </form>
              
              <div style={{ position: 'relative' }}>
                <span
                  onClick={()=>setIsShowPassword(!isShowPassword)}
                  style={{
                    zIndex: 10,
                    position: 'absolute',
                    top: '4px',
                    right: '8px'
                  }}
                >
                  {
                  isShowPassword ? (
                    <EyeFilled />
                  ) : (
                    <EyeInvisibleFilled />
                  )
                }
                </span>
                <InputForm
                  placeholder="password"
                  type={isShowPassword ? "text" : "password"}
                  value={password}
                  onChange={handleOnchangePassword}
                />
              </div>
              {data?.status === 'ERR' && <span style={{ color: 'red' }}>{data?.message}</span>}
              <Loading isLoading={isLoading}>
                <ButtonComponent
                    disabled={!email.length || !password.length}
                    onClick={handleSignIn}
                    size={40}
                    styleButton={{
                      background: 'rgb(255, 57, 69)',
                      height: '48px',
                      width: '100%',
                      border: 'none',
                      borderRadius: '4px',
                      margin: '26px 0 10px'
                  }}
                  textbutton={'Đăng nhập'}
                  styleTextButton={{ color: '#fff', fontSize: '15px', fontWeight: '700' }}
                >
                </ButtonComponent>
              </Loading>
              <p><WrapperTextLight>Quên mật khẩu?</WrapperTextLight></p>
              <p>Bạn chưa có tài khoản? <WrapperTextLight onClick={handleNavigateSignUp}> Tạo tài khoản</WrapperTextLight></p>
            </WrapperContainerLeft>

            <WrapperContainerRight>
              <Image src={imageLogo} preview={false} alt="iamge-logo" height="203px" width="203px" />
              <h4>Mua sắm tại LTTD</h4>
            </WrapperContainerRight>
        </div>
      </div >
  )
}
  
export default SignInPage