import styled from "styled-components";
// import { Button } from 'antd';
import ButtonComponent from "../../components/ButtonComponent/ButtonComponent";

export const WrapperTypeProduct = styled.div`
    display: flex;
    align-items: center;
    gap: 24px;
    justify-content: flex-start;
    // border-bottom: 1px solid blue;
    height: 44px;
`
{/* 
export const WrapperButtonViewMore = styled(Button)`
     color: rgb(11, 116, 229);
     background: white; 
     border-color: rgb(13, 92, 182);
     width: 240px;
     height: 38px;

     &:hover {
         background-color: rgb(13, 92, 182) !important;
         color: white !important;
         border: none;
         fontWeight: 500
     }
 `
*/}
export const WrapperButtonMore = styled(ButtonComponent)`
    &:hover {
        color: #fff;
        background: #9255FD;
        span {
            color: #fff;
        }
    }
    width: 100%;
    color: #9255FD;
    text-align: center;
    cursor: ${(props) => props.disabled ? 'not-allowed' : 'pointers'}
`

export const WrapperProducts = styled.div`
    display: flex;
    gap: 14px;
    margin-top:20px;
    flex-wrap: wrap;
`