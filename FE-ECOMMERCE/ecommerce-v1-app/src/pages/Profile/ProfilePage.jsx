import React, { useEffect, useState } from 'react'
import { UploadOutlined} from '@ant-design/icons'
import ButtonComponent from '../../components/ButtonComponent/ButtonComponent'
import InputForm from '../../components/InputForm/InputForm'
import { WrapperContentProfile, 
    WrapperHeader, 
    WrapperInput,
    WrapperLabel,
    WrapperUploadFile
} from './Style'
import { Button } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import * as UserService from '../../services/UserService'
import { useMutationHooks } from '../../hooks/useMutationHook'
import Loading from '../../components/LoadingComponent/Loading'
import * as message from '../../components/Message/Message'
import { updateUser } from '../../redux/slices/UserSlice'
import { getBase64 } from '../../utils'

const ProfilePage = () => {
    const user = useSelector((state) => state.user)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [address, setAddress] = useState('')
    const [avatar, setAvatar] = useState('')
    const mutation = useMutationHooks(
        (data) => {
            const { id, access_token, ...rests } = data
            UserService.updateUser(id, rests, access_token)
        }
    )

    const dispatch = useDispatch()
    const { data, isLoading, isSuccess, isError } = mutation

    useEffect(() => {
        setName(user?.name)
        setEmail(user?.email)
        setPhone(user?.phone)
        setAddress(user?.address)
        setAvatar(user?.avatar)
    }, [user])

    useEffect(() => {
        if (isSuccess) {
            message.success("Updated success")
            handleGetDetailsUser(user?.id, user?.access_token)
        } else if (isError) {
            message.error()
        }
    }, [isSuccess, isError])

    const handleGetDetailsUser = async (id, token) => {
        const res = await UserService.getDetailsUser(id, token)
        dispatch(updateUser({ ...res?.data, access_token: token }))
    }

    const handleOnchangeEmail = (value) => {
        setEmail(value)
    }
    const handleOnchangeName = (value) => {
        setName(value)
    }
    const handleOnchangePhone = (value) => {
        setPhone(value)
    }
    const handleOnchangeAddress = (value) => {
        setAddress(value)
    }

    const handleOnChangeAvatar = async ({ fileList }) => {
        const file = fileList[0];
        if (!file.url && !file.preview) {
            // Tạo preview bằng cách chuyển đổi file thành base64
            const preview = await getBase64(file.originFileObj);
            // Cập nhật thông tin file với preview
            file.preview = preview;
        }
        // Cập nhật trạng thái avatar với preview của file
        setAvatar(file.preview);
    }

    const handleUpdate = () => {
        mutation.mutate({ id: user?.id, email, name, phone, address, avatar, access_token: user?.access_token })
    }

  return (
    <div style={{ width: '1270px', margin: '0 auto', height: '500px' }}>
        <WrapperHeader>Thông tin người dùng</WrapperHeader>
        <WrapperContentProfile>
            <WrapperInput>
                <WrapperLabel htmlFor="name">Name</WrapperLabel>
                <InputForm style={{ width: '300px' }} id="name" value={name} onChange={handleOnchangeName} />
                <ButtonComponent
                    onClick={handleUpdate}
                    size={40}
                    styleButton={{
                    height: '30px',
                    width: 'fit-content',
                    borderRadius: '4px',
                    padding: '2px 6px 6px'
                    }}
                    textbutton={'Cập nhật'}
                    styleTextButton={{ color: 'rgb(26, 148, 255)', fontSize: '15px', fontWeight: '700' }}
                    ></ButtonComponent>
            </WrapperInput>
            <WrapperInput>
                <WrapperLabel htmlFor="Email">Email</WrapperLabel>
                <InputForm style={{ width: '300px' }} id="email" value={email} onChange={handleOnchangeEmail} />
                <ButtonComponent
                    onClick={handleUpdate}
                    size={40}
                    styleButton={{
                    height: '30px',
                    width: 'fit-content',
                    borderRadius: '4px',
                    padding: '2px 6px 6px'
                    }}
                    textbutton={'Cập nhật'}
                    styleTextButton={{ color: 'rgb(26, 148, 255)', fontSize: '15px', fontWeight: '700' }}
                    ></ButtonComponent>
            </WrapperInput>
            <WrapperInput>
                <WrapperLabel htmlFor="phone">Phone</WrapperLabel>
                <InputForm style={{ width: '300px' }} id="phone" value={phone} onChange={handleOnchangePhone} />
                <ButtonComponent
                    onClick={handleUpdate}  
                    size={40}
                    styleButton={{
                    height: '30px',
                    width: 'fit-content',
                    borderRadius: '4px',
                    padding: '2px 6px 6px'
                    }}
                    textbutton={'Cập nhật'}
                    styleTextButton={{ color: 'rgb(26, 148, 255)', fontSize: '15px', fontWeight: '700' }}
                ></ButtonComponent>
            </WrapperInput>
            <WrapperInput>
                <WrapperLabel htmlFor="avatar">Avatar</WrapperLabel>
                <WrapperUploadFile onChange={handleOnChangeAvatar} maxCount={1}>
                    <Button icon={<UploadOutlined />}>Select File</Button>
                </WrapperUploadFile>
                {avatar && (
                            <img src={avatar} style={{
                                height: '60px',
                                width: '60px',
                                borderRadius: '50%',
                                objectFit: 'cover'
                            }} alt="avatar"/>
                        )}
                <ButtonComponent
                    onClick={handleUpdate}
                    size={40}
                    styleButton={{
                    height: '30px',
                    width: 'fit-content',
                    borderRadius: '4px',
                    padding: '2px 6px 6px'
                    }}
                    textbutton={'Cập nhật'}
                    styleTextButton={{ color: 'rgb(26, 148, 255)', fontSize: '15px', fontWeight: '700' }}
                ></ButtonComponent>
            </WrapperInput>
            <WrapperInput>
                <WrapperLabel htmlFor="address">Address</WrapperLabel>
                <InputForm style={{ width: '300px' }} id="address" value={address} onChange={handleOnchangeAddress} />
                <ButtonComponent
                    onClick={handleUpdate}
                    size={40}
                    styleButton={{
                    height: '30px',
                    width: 'fit-content',
                    borderRadius: '4px',
                    padding: '2px 6px 6px'
                    }}
                    textbutton={'Cập nhật'}
                    styleTextButton={{ color: 'rgb(26, 148, 255)', fontSize: '15px', fontWeight: '700' }}
                ></ButtonComponent>
            </WrapperInput>
        </WrapperContentProfile>
    </div>
  )
}

export default ProfilePage